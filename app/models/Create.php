<?php
/**
 * Method create api
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace App\models;

use App\core\Model;

class Create extends Model
{
    /**
     * @return void
     */
    public function index(): void
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        $product = new self;
        $data = json_decode(file_get_contents("php://input"));
        if (!empty($data->name) &&
            !empty($data->price) &&
            !empty($data->description) &&
            !empty($data->category_id)
        ) {
            $product->name = $data->name;
            $product->price = $data->price;
            $product->description = $data->description;
            $product->category_id = $data->category_id;
            $product->created = date("Y-m-d H:i:s");
            if ($product->create()) {
                http_response_code(201);
                echo json_encode(array("message" => "Товар был создан."), JSON_UNESCAPED_UNICODE);
            } else {
                http_response_code(503);
                echo json_encode(array("message" => "Невозможно создать товар."), JSON_UNESCAPED_UNICODE);
            }
        } else {
            http_response_code(400);
            echo json_encode(array("message" => "Невозможно создать товар. Данные неполные."), JSON_UNESCAPED_UNICODE);
        }
    }
}
