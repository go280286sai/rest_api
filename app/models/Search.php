<?php
/**
 * Method search api
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
namespace App\models;

use App\core\Model;

class Search extends Model
{
    /**
     * @return void
     */
    public function index(): void
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        $product = new self;
        $keywords = $_GET["s"] ?? "";
        $stmt = $product->search($keywords);
        $num = count($stmt);
        if ($num > 0) {
            $products_arr = array();
            $products_arr["records"] = array();
            foreach ($stmt as $item) {
                extract($item);
                $product_item = array(
                    "id" => $id??'',
                    "name" => $name??'',
                    "description" => html_entity_decode($description??''),
                    "price" => $price??'',
                    "category_id" => $category_id??'',
                    "category_name" => $category_name??''
                );
                $products_arr["records"][] = $product_item;
            }
            http_response_code(200);
            echo json_encode($products_arr);
        } else {
            http_response_code(404);
            echo json_encode(array("message" => "Товары не найдены."), JSON_UNESCAPED_UNICODE);
        }
    }
}
