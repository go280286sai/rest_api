<?php
/**
 * Method Update api
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
namespace App\models;

use App\core\Model;

class Update extends Model
{
    /**
     * @return void
     */
    public function index(): void
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        $product = new self;
        $data = json_decode(file_get_contents("php://input"));
        $product->id = $data->id;
        $product->name = $data->name;
        $product->price = $data->price;
        $product->description = $data->description;
        $product->category_id = $data->category_id;
        if ($product->update()) {
            http_response_code(200);
            echo json_encode(array("message" => "Товар был обновлён."), JSON_UNESCAPED_UNICODE);
        } else {
            http_response_code(503);
            echo json_encode(array("message" => "Невозможно обновить товар."), JSON_UNESCAPED_UNICODE);
        }
    }
}
