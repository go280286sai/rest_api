<?php
/**
 * Method One api
 */
namespace App\models;

use App\core\Model;

class One extends Model
{
    /**
     * @return void
     */
    public function index(): void
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: access");
        header("Access-Control-Allow-Methods: GET");
        header("Access-Control-Allow-Credentials: true");
        header("Content-Type: application/json");
        $product = new self;
        if (isset($_GET["id"])) {
            $product->id = $_GET["id"];
        } else {
            die();
        }
        $product->readOne();
        if ($product->name != null) {
            $product_arr = array(
                "id" => $product->id,
                "name" => $product->name,
                "description" => $product->description,
                "price" => $product->price,
                "category_id" => $product->category_id,
                "category_name" => $product->category_name
            );
            http_response_code(200);
            echo json_encode($product_arr);
        } else {
            http_response_code(404);
            echo json_encode(array("message" => "Товар не существует."), JSON_UNESCAPED_UNICODE);
        }
    }
}
