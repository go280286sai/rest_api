<?php

/**
 * class app
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
namespace App;

require dirname(__DIR__) . '/vendor/autoload.php';

use App\core\Exception;
use App\core\Routes;

class App
{
    public function __construct()
    {
        new Routes();
    }
}
