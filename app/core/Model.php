<?php
/**
 * class Model
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace App\core;

use Flight;
use PDO;

class Model
{
    /**
     * @var object
     */
    public object $conn;
    /**
     * @var object
     */
    public object $db;
    /**
     * @var string
     */
    public string $table_name = "products";
    /**
     * @var string
     */
    public string $id;
    /**
     * @var string
     */
    public string $name;
    /**
     * @var string
     */
    public string $description;
    /**
     * @var string
     */
    public string $price;
    /**
     * @var string
     */
    public string $category_id;
    /**
     * @var string
     */
    public string $category_name;
    /**
     * @var string
     */
    public string $created;

    public function __construct()
    {
        $this->conn = Flight::db();
    }

    /**
     * @return bool
     */
    public function create(): bool
    {
        // запрос для вставки (создания) записей
        $query = "INSERT INTO
                " . $this->table_name . "
            SET
                name=:name, price=:price, description=:description, category_id=:category_id, created=:created";
        $stmt = $this->conn->prepare($query);
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->price = htmlspecialchars(strip_tags($this->price));
        $this->description = htmlspecialchars(strip_tags($this->description));
        $this->category_id = htmlspecialchars(strip_tags($this->category_id));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":category_id", $this->category_id);
        $stmt->bindParam(":created", $this->created);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /**
     * @return void
     */
    public function readOne(): void
    {
        // запрос для чтения одной записи (товара)
        $query = "SELECT c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created
            FROM $this->table_name p LEFT JOIN categories c ON p.category_id = c.id
            WHERE p.id = ? LIMIT 0,1";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (empty($row)) {
            die('Нет такого товара');
        }
        $this->name = $row["name"];
        $this->price = $row["price"];
        $this->description = $row["description"];
        $this->category_id = $row["category_id"];
        $this->category_name = $row["category_name"];
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        $query = "UPDATE $this->table_name 
            SET
                name = :name,
                price = :price,
                description = :description,
                category_id = :category_id
            WHERE
                id = :id";
        $stmt = $this->conn->prepare($query);
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->price = htmlspecialchars(strip_tags($this->price));
        $this->description = htmlspecialchars(strip_tags($this->description));
        $this->category_id = htmlspecialchars(strip_tags($this->category_id));
        $this->id = htmlspecialchars(strip_tags($this->id));
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":category_id", $this->category_id);
        $stmt->bindParam(":id", $this->id);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /**
     * @param $keywords
     * @return mixed
     */
    public function search($keywords): mixed
    {
        $keywords = htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";
        $query = "SELECT
                c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created
            FROM
                 $this->table_name  p
                LEFT JOIN
                    categories c
                        ON p.category_id = c.id
            WHERE
                p.name LIKE '$keywords' OR p.description LIKE '$keywords' OR c.name LIKE '$keywords'
            ORDER BY
                p.created DESC";
        $stmt = $this->conn->query($query)->fetchAll(PDO::FETCH_ASSOC);

        return $stmt;
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
        $stmt = $this->conn->prepare($query);
        $this->id = htmlspecialchars(strip_tags($this->id));
        $stmt->bindParam(1, $this->id);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function read(): mixed
    {
        // выбираем все записи
        $query = "SELECT c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created
            FROM
                $this->table_name p
                LEFT JOIN
                    categories c
                        ON p.category_id = c.id
            ORDER BY
                p.created DESC";
        $stmt = $this->conn->query($query)->fetchAll();
        return $stmt;
    }
}
