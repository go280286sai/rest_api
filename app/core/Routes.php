<?php

namespace App\core;

use App\controllers\ApiController;
use App\controllers\MainController;
use Flight;
use PDO;

class Routes
{
    public function __construct()
    {
        $mainController = new MainController();
        Flight::register('db', 'PDO', array('mysql:host=' . $_ENV['DB_HOST'] . ';port=' . $_ENV['DB_PORT'] .
            ';dbname=' . $_ENV['DB_DATABASE'], $_ENV['DB_USERNAME'], $_ENV['DB_PASSWORD']), function ($db) {
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            });
        $apiController = new ApiController();
        Flight::route('/', array($mainController, 'index'));
        Flight::route('/create', array($mainController, 'create'));
        Flight::route('/update', array($mainController, 'update'));
        Flight::route('/search', array($mainController, 'search'));
        Flight::route('/api_read', array($apiController, 'read'));
        Flight::route('/api_one/*', array($apiController, 'one'));
        Flight::route('/api_delete/*', array($apiController, 'delete'));
        Flight::route('/api_update/*', array($apiController, 'update'));
        Flight::route('/api_create/*', array($apiController, 'create'));
        Flight::route('/api_search/*', array($apiController, 'search'));

        Flight::start();
    }
}
