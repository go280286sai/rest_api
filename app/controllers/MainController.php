<?php
/**
 * MainController
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
namespace App\controllers;

use Flight;

class MainController
{
    /**
     * @return void
     */
    public function index(): void
    {
        Flight :: set('flight.views.path', dirname(__DIR__).'/views');
        Flight :: render('header.php', array ( 'heading' => '' ), 'header_content');
        Flight :: render('index', array ( 'body' => '' ), 'body_content');
        Flight :: render('layout.php', array ( 'title' => 'API interface' ));
    }

    /**
     * @return void
     */
    public function create(): void
    {
        Flight :: set('flight.views.path', dirname(__DIR__).'/views');
        Flight :: render('header.php', array ( 'heading' => '' ), 'header_content');
        Flight :: render('create', array ( 'body' => '' ), 'body_content');
        Flight :: render('layout.php', array ( 'title' => 'API interface' ));
    }

    /**
     * @return void
     */
    public function update(): void
    {
        Flight :: set('flight.views.path', dirname(__DIR__).'/views');
        Flight :: render('header.php', array ( 'heading' => '' ), 'header_content');
        Flight :: render('update', array ( 'body' => '' ), 'body_content');
        Flight :: render('layout.php', array ( 'title' => 'API interface' ));
    }

    /**
     * @return void
     */
    public function search(): void
    {
        Flight :: set('flight.views.path', dirname(__DIR__).'/views');
        Flight :: render('header.php', array ( 'heading' => '' ), 'header_content');
        Flight :: render('search', array ( 'body' => '' ), 'body_content');
        Flight :: render('layout.php', array ( 'title' => 'API interface' ));
    }
}
