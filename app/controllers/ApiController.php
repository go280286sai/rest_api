<?php
/**
 * API Controllers
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
namespace App\controllers;

use App\models\Create;
use App\models\Delete;
use App\models\One;
use App\models\Read;
use App\models\Search;
use App\models\Update;

class ApiController
{
    /**
     * @return void
     */
    public function read(): void
    {
        $class = new Read();
        $class->index();
    }

    /**
     * @return void
     */
    public function one(): void
    {
        $class = new One();
        $class->index();
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        $class = new Delete();
        $class->index();
    }

    /**
     * @return void
     */
    public function update(): void
    {
        $class = new Update();
        $class->index();
    }

    /**
     * @return void
     */
    public function create(): void
    {
        $class = new Create();
        $class->index();
    }

    /**
     * @return void
     */
    public function search(): void
    {
        $class = new Search();
        $class->index();
    }
}
