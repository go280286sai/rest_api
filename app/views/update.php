<body xmlns="http://www.w3.org/1999/html">
<div class="container tables">
    <h1>Update product</h1>
    <form action="http://localhost:8184/api_update" method="post" class="border font-monospace">
        <div class="mb-3">
            <label class="form-label">name</label>
            <input type="text" class="form-control" name="name" value="">
        </div>
        <div class="mb-3">
            <label class="form-label">price</label>
            <input type="number" class="form-control" name="price" value="">
        </div>

        <div class="mb-3">
            <label for="floatingTextarea">Short description</label>
            <textarea class="form-control" rows="3" name="short"></textarea>
        </div>
        <div class="mb-3">
            <label class="form-label">Category</label>
            <input type="number" class="form-control" name="category" value="">
        </div>
        <br>
        <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Submit</button>
        <button type="button" class="btn btn-primary" name="main" value="main" id="btn_5"
                onclick="window.open('http://localhost:8184/', '_self')">Main
        </button>
    </form>
    <div class="success"></div>
    <script>
        $(document).ready(function () {
            $.ajax({
                url: "http://localhost:8184/api_one?id=<?php echo $_POST['id']?>",
                method: 'POST',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    console.log(data);
                    $('input').eq(0).val(`${data['name']}`);
                    $('input').eq(1).val(`${data['price']}`);
                    $('textarea').val(`${data['description']}`);
                    $('input').eq(2).val(`${data['category_id']}`);
                }
            });
        });
        $('form').submit(function (event) {
            event.preventDefault();
            let $form = $(this),
                id=<?php echo $_POST['id']?>;
                name = $form.find("input[name='name']").val(),
                price = $form.find("input[name='price']").val(),
                description = $form.find("textarea[name='short']").val(),
                category_id = $form.find("input[name='category']").val(),
                url = 'http://localhost:8184/api_update';
            $.post(url, JSON.stringify({id:id, name: name, price: price, description: description, category_id: category_id}),
                function (data, status) {
                    $('.success').text("Data: " + data + "\nStatus: " + status);
                });
            $('input').eq(0).val('');
            $('input').eq(1).val('');
            $('textarea').val('');
            $('input').eq(2).val('');
            $(location).attr('href','http://localhost:8184');
        })

    </script>
</body>
</html>