<body>
<div class="container tables">
    <form action="" method="post" class="border font-monospace">
        <div class="mb-3">
            <label class="form-label">Search text</label>
            <input type="text" class="form-control" name="name">
        </div>
        <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Submit</button>
        <button type="button" class="btn btn-primary" name="main" value="main" id="btn_5"
                onclick="window.open('http://localhost:8184/', '_self')">Main
        </button>
    </form>
    <div class="success"></div>
    <script>
        $('form').on('submit', function (e) {
            e.preventDefault();
            let name = $('form').find("input[name='name']").val();
            $.get(`http://localhost:8184/api_search?s=${name}`, function (data) {
                    console.log(data);
                    data = data['records'];
                    let len = data.length;
                    let table_td = `<tr>
                        <th>id</th><th>name</th><th>price</th><th>description</th><th>category_id</th><th>update</th><th>delete</th>
                    </tr>`;
                    for (let i = 0; i < len; i++) {
                        table_td += `<tr>`;
                        table_td += `<td>${data[i]['id']}</td>`;
                        table_td += `<td>${data[i]['name']}</td>`;
                        table_td += `<td>${data[i]['price']}</td>`;
                        table_td += `<td>${data[i]['description']}</td>`;
                        table_td += `<td>${data[i]['category_id']}</td>`;
                        table_td += `<td><form action="http://localhost:8184/update" method="post"><input type="text"  hidden name="id" value="${data[i]['id']}"><input type="submit" class="btn btn-primary" value="Update"></form></td>`;
                        table_td += `<td><input type="button" class="btn btn-primary" onclick="del(${data[i]['id']})" value="Delete"></td>`;
                        table_td += `</tr>`;
                    }
                    $(".success").html(`<table class="table table-success table-striped">${table_td}</table>`);
                })
            });
        function del(name_id) {
            url = 'http://localhost:8184/api_delete';
            $.post(url, JSON.stringify({id:name_id}),
                function (data, status) {
                    $('.success').text("Data: " + data + "\nStatus: " + status);
                });
            window.location.replace("http://localhost:8184");
        }
    </script>
</body>