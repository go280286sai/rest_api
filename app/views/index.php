  <div class="container">
      <form action='http://localhost:8184/create' method="post" class="border font-monospace">
          <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Create</button>
          <button type="button" class="btn btn-primary" name="main" value="main" id="btn_5"
                  onclick="window.open('http://localhost:8184/search', '_self')">Search
          </button>
      </form>
        <div class="success"></div>
  </div>
        <script>
            $(document).ready(function () {
                $.ajax({
                    url: 'http://localhost:8184/api_read',
                    method: 'POST',
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function (data) {
                        data = data['records'];
                        let len = data.length;
                        let table_td = `<tr>
                        <th>id</th><th>name</th><th>price</th><th>description</th><th>category_id</th><th>update</th><th>delete</th>
                    </tr>`;
                        for (let i = 0; i < len; i++) {
                            table_td += `<tr>`;
                            table_td += `<td>${data[i]['id']}</td>`;
                            table_td += `<td>${data[i]['name']}</td>`;
                            table_td += `<td>${data[i]['price']}</td>`;
                            table_td += `<td>${data[i]['description']}</td>`;
                            table_td += `<td>${data[i]['category_id']}</td>`;
                            table_td += `<td><form action="http://localhost:8184/update" method="post"><input type="text"  hidden name="id" value="${data[i]['id']}"><input type="submit" class="btn btn-primary" value="Update"></form></td>`;
                            table_td += `<td><input type="button" class="btn btn-primary" onclick="del(${data[i]['id']})" value="Delete"></td>`;
                            table_td += `</tr>`;
                        }
                        $(".success").html(`<table class="table table-success table-striped">${table_td}</table>`);
                    }
                });
            })
            function del(name_id) {
                url = 'http://localhost:8184/api_delete';
                $.post(url, JSON.stringify({id:name_id}),
                    function (data, status) {
                        $('.success').text("Data: " + data + "\nStatus: " + status);
                    });
                window.location.replace("http://localhost:8184");
            }
        </script>
    </div>
</body>