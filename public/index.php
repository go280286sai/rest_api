<?php

require '../vendor/autoload.php';

use App\App;
use Symfony\Component\Dotenv\Dotenv;

define('ROOT', dirname(__DIR__));
$dotenv = new Dotenv();
$dotenv->load(dirname(__DIR__) . '/.env');
new App();
